import openpyxl
import os
import os.path
import xml.etree.ElementTree as ET
import shutil

# wb_file =  'C:\\Dropbox (Movimento GM Onsite)\\Altero\\MGNA0940_Ford_GT_Multi-Module\\Documents\\PUMA docs\\Customer car software update process rev7.xlsx'
# wb_file = 'C:\\Users\\akanarek\\Desktop\\Movimento\\Projects\\editECDLogs\\Customer car software update process rev8 (003).xlsx'
wb_file = 'C:\\Users\\akanarek\Desktop\\Movimento\\Projects\\editecdlogs\\VINS.xlsx'
source_dir = 'C:\\Users\\akanarek\\Desktop\\Movimento\\Projects\\editecdlogs\\TestFiles\\'
# source_dir = 'C:\\Dropbox (Movimento GM Onsite)\\Altero\\MGNA0940_Ford_GT_Multi-Module\\Engineering\\0. Customer Software\\converted ECD\\converted ECD\\converted ECD IPC and APIM\\'
destination_dir = 'C:\\Users\\akanarek\\Desktop\\Movimento\\Projects\\editecdlogs\\TestFiles-Edited\\'
# destination_dir = 'C:\\Dropbox (Movimento GM Onsite)\\Altero\\MGNA0940_Ford_GT_Multi-Module\\Engineering\\0. Customer Software\\converted ECD\\converted ECD\\converted ECD IPC and APIM - Edited2\\'


sheetname = 'VINS'
row_start = 2
vin_col = 1
apimna_col = 12
apimeu_col = 13
apimgcc_col = 14
ipcdev_col = 20
ipcprod_col = 21
vin_length = 17


dir_vin_list = []
excel_vin_list = []




def findDiff():
	print("Extra files in Dir")
	for vin in dir_vin_list:
		if vin not in excel_vin_list:
			print(vin)

	print("Extra files in excel")
	for vin in excel_vin_list:
		if vin not in dir_vin_list:
			print(vin)


def setDirVinList():
	for filename in os.listdir(source_dir):
		dir_vin_list.append(getFilenameVIN(filename))

def getFilenameVIN(filename):
	base_filename = os.path.splitext(filename)[0];
	if len(base_filename) >= vin_length:
		vin = base_filename[-vin_length:]
		return vin
	return base_filename


def findCopyEditFile(vin, apim_name, ipc_name):
	for filename in os.listdir(source_dir):
		filename_vin = getFilenameVIN(filename)

		
		if (filename_vin == vin):
			# print("vin: " + vin + ", filename_vin: " + filename_vin + ", status: " + status);
			# read xml doc
			xml_tree = ET.parse(source_dir + filename)
			root = xml_tree.getroot()

			config_el_list = root.findall('CONFIGURATION');
			if len(config_el_list) < 2:
				return

			name1 = config_el_list[0].find('NAME')
			name2 = config_el_list[1].find('NAME')

			if (name1 is None) or (name2 is None):
				return


			name1.text = apim_name
			name2.text = ipc_name
			xml_tree.write(destination_dir + filename)

			return



def editECDLogs():

	wb = openpyxl.load_workbook(wb_file)

	sheet = wb.get_sheet_by_name(sheetname)


	vin_header_cell = sheet.cell(row=1, column=vin_col).value
	apimna_header_cell = sheet.cell(row=1, column=apimna_col).value
	apimeu_header_cell = sheet.cell(row=1, column=apimeu_col).value
	apimgcc_header_cell = sheet.cell(row=1, column=apimgcc_col).value
	ipcdev_header_cell = sheet.cell(row=1, column=ipcdev_col).value
	ipcprod_header_cell = sheet.cell(row=1, column=ipcprod_col).value

	if not (
		vin_header_cell == "VIN" and 
		apimna_header_cell == "APIMna" and 
		apimeu_header_cell == "APIMeu" and
		apimgcc_header_cell == "APIMgcc" and
		ipcdev_header_cell == "IPCDev" and
		ipcprod_header_cell == "IPCProd"
		):
		print(sheetname);


	# create destination directory if it doesnt exist
	if not os.path.exists(destination_dir):
	    os.makedirs(destination_dir)


	# get first VIN
	vin = sheet.cell(row=row_start, column=vin_col).value
	counter = row_start
	# loop through VINs
	while vin is not None and vin != "":
		excel_vin_list.append(vin)
		# print("vin: " + vin)
		# check length of VIN in Excel - should be vin_length char
		if (len(vin) != vin_length):
			print("Length of VIN in Excel is not " + str(vin_length) + " characters for VIN: " + vin)

		apimna = sheet.cell(row=counter, column=apimna_col).value
		apimeu = sheet.cell(row=counter, column=apimeu_col).value
		apimgcc = sheet.cell(row=counter, column=apimgcc_col).value
		ipcdev = sheet.cell(row=counter, column=ipcdev_col).value
		ipcprod = sheet.cell(row=counter, column=ipcprod_col).value

		apim_name = ''
		ipc_name = ''

		if (apimna.strip() != ""):
			apim_name = apimna.strip()
		elif (apimeu.strip() != ''):
			apim_name = apimeu.strip()
		elif (apimgcc.strip() != ''):
			apim_name = apimgcc.strip()
		else:
			apim_name = "APIM"

		if (ipcdev.strip() != ''):
			ipc_name = ipcdev.strip()
		elif (ipcprod.strip() != ''):
			ipc_name = ipcprod
		else:
			print(vin)
			return

		# print("apim_name: " + apim_name)
		# print("ipc_name: " + ipc_name)

		
		findCopyEditFile(vin, apim_name, ipc_name)


		

		counter += 1
		vin = sheet.cell(row=counter, column=vin_col).value

	print("count: " + str(counter-row_start))


	# setDirVinList()
	# findDiff()






if __name__ == "__main__":


	editECDLogs()

	print("Process Complete")




