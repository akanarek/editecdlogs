import openpyxl
import os
import os.path
import xml.etree.ElementTree as ET
import shutil

# wb_file =  'C:\\Dropbox (Movimento GM Onsite)\\Altero\\MGNA0940_Ford_GT_Multi-Module\\Documents\\PUMA docs\\Customer car software update process rev7.xlsx'
wb_file = 'C:\\Users\\akanarek\\Desktop\\Movimento\\Projects\\editECDLogs\\Customer car software update process rev8 (003).xlsx'
# source_dir = 'C:\\Dropbox (Movimento GM Onsite)\\Altero\\MGNA0940_Ford_GT_Multi-Module\\Engineering\\1. Flash\\FILES\\'
source_dir = 'C:\\Dropbox (Movimento GM Onsite)\\Altero\\MGNA0940_Ford_GT_Multi-Module\\Engineering\\0. Customer Software\\converted ECD\\converted ECD\\converted ECD IPC and APIM\\'
# destination_dir = 'C:\\Dropbox (Movimento GM Onsite)\\Altero\\MGNA0940_Ford_GT_Multi-Module\\Engineering\\1. Flash\\FILES-EDITED\\'
destination_dir = 'C:\\Dropbox (Movimento GM Onsite)\\Altero\\MGNA0940_Ford_GT_Multi-Module\\Engineering\\0. Customer Software\\converted ECD\\converted ECD\\converted ECD IPC and APIM - Edited2\\'

# sheetname = 'Cluster key'
# row_start = 2
# vin_col = 1
# status_col = 3
# vin_length = 7

sheetname = 'Special Considerations'
row_start = 5
vin_col = 3
status_col = 9
vin_length = 8


dir_vin_list = []
excel_vin_list = []




def findDiff():
	print("Extra files in Dir")
	for vin in dir_vin_list:
		if vin not in excel_vin_list:
			print(vin)

	print("Extra files in excel")
	for vin in excel_vin_list:
		if vin not in dir_vin_list:
			print(vin)


def setDirVinList():
	for filename in os.listdir(source_dir):
		dir_vin_list.append(getFilenameVIN(filename))

def getFilenameVIN(filename):
	base_filename = os.path.splitext(filename)[0];
	if len(base_filename) >= vin_length:
		vin = base_filename[-vin_length:]
		return vin
	return base_filename


def findCopyEditFile(vin, status):
	for filename in os.listdir(source_dir):
		filename_vin = getFilenameVIN(filename)

		
		if (filename_vin == vin):
			# print("vin: " + vin + ", filename_vin: " + filename_vin + ", status: " + status);
			# read xml doc
			xml_tree = ET.parse(source_dir + filename)
			root = xml_tree.getroot()

			# read and update Name element
			name = root.find('CONFIGURATION').find('NAME');
			if name is None:
				print("ERROR: No 'Name' element for file '" +  source_dir + filename + "'")
			else:
				name.text = status
				xml_tree.write(destination_dir + filename)
			return



def editECDLogs():

	wb = openpyxl.load_workbook(wb_file)

	sheet = wb.get_sheet_by_name(sheetname)


	vin_header_cell = sheet.cell(row=1, column=1).value
	sns_header_cell = sheet.cell(row=1, column=2).value
	status_header_cell = sheet.cell(row=1, column=3).value

	if not (vin_header_cell == "VIN" and sns_header_cell == "SN's" and status_header_cell == "STATUS"):
		print(sheetname);


	# create destination directory if it doesnt exist
	if not os.path.exists(destination_dir):
	    os.makedirs(destination_dir)


	# get first VIN
	vin = sheet.cell(row=row_start, column=vin_col).value
	counter = row_start
	# loop through VINs
	while vin is not None and vin != "":
		excel_vin_list.append(vin)
		# print("vin: " + vin)
		# check length of VIN in Excel - should be vin_length char
		if (len(vin) != vin_length):
			print("Length of VIN in Excel is not " + str(vin_length) + " characters for VIN: " + vin)


		status = sheet.cell(row=counter, column=status_col).value
		if status == "Clusterdev":
			status = "IPCDev"
		elif status == "Cluster":
			status = "IPCProd"
		else:
			print("Invalid Status in Excel sheet: " + str(status))


		
		findCopyEditFile(vin, status)


		

		counter += 1
		vin = sheet.cell(row=counter, column=vin_col).value

	print("count: " + str(counter-row_start))


	setDirVinList()
	findDiff()






if __name__ == "__main__":


	editECDLogs()

	print("Process Complete")




